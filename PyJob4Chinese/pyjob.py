# encoding: utf-8
__author__ = 'pybeaner'

import urllib, urllib2
import json
from bs4 import BeautifulSoup
from random import choice

from constant import *

API_BASE = 'http://opendata.baidu.com/api.php?'
RESOURCE_ID = 6666
FROM_MID = 1


def get(query='', **kwargs):

    payload = kwargs
    if query:
        payload.update({
            'resource_id': RESOURCE_ID,
            'from_mid':1,
            'query':query,
        })
    params = urllib.urlencode(payload)
    api_url = API_BASE + params
    f = urllib2.urlopen(api_url)
    bs = BeautifulSoup(f.read())
    return bs.get_text()


if __name__ == '__main__':
    default_query_key_list = ['python', 'linux', 'java']
    query = choice(default_query_key_list)
    job_edu = JOB_EDU.get(choice(JOB_EDU.keys()))
    job_sal = JOB_SAL.get(choice(JOB_SAL.keys()))
    result = get(query=query, job_edu=job_edu, job_sal=job_sal)
    print(result)